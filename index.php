
<!DOCTYPE html>
<html lang="en">
<head>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-188329729-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-188329729-1');
    </script>
    <link rel="stylesheet" href="./css/cssGrid.css">
    <link rel="stylesheet" href="./css/mediaQuery.css">
    <!-- <link rel="stylesheet" href="./css/navbar.css">
    <link rel="stylesheet" href="./css/bootstrapcore.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbars/">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <style>
        /* Make the image fully responsive */
        .carousel-inner img {
            width: 100%;
            height: 770px;
        }
        @media (min-width:320px) and (max-width:553px){
            .carousel-inner img {
            width: 100%;
            height: 100%;
        }
    }
  </style>
  
    <title>3WE HORSE</title>
</head>
<body>

    <div class="mainwrapper">
        <div class="wrapper">
        <div class="bghead">
            <div class="container" >
                <div class="row py-4 mt-4 "  >
                    <div class="col-sm-6 header-login" id="thiss">
                        <img src="images/logo.svg" alt="">
                    </div>
                    <div class="col-sm-6 header-login" id="thiss">
                        <div class="row">
                            <div class="col-8">
                                <div class="row no-gutters">
                                    <div class="col-6 pr-2">
                                        <input type="text" name="email" id="emaillogin" class="form-control" placeholder="Username"  style="background-color: #4b515f; color: white" />
                                    </div>
                                    <div class="col-6 pr-2">
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="password" name="password" id="passwordlogin" class="form-control" placeholder="Password" style="background-color: #4b515f;  color: white" />
                                            </div>
                                            <div class="col-12 mt-1">
                                                <!-- <small style="color: gray; cursor: pointer">Forgot Password?</small> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class=" row no-gutters">
                                    <div class="col-6 pr-2 header-login-buttons-login">
                                        <button type="button" class="btn btn" id="loginme"  data-toggle="modal" data-target="#Login">
                                            <i style="color: white; font-style: unset">
                                                <img  src="images/loginvector.png" alt="">
                                                Log In
                                            </i>
                                        </button>
                                    </div>
                                    <div class="col-6 header-login-buttons-join">
                                        <button type="button" class="btn btn" data-toggle="modal" data-target="#Joinus">
                                            <i style="color: white; font-style: unset">
                                                <img src="images/joinnow.png" alt="">
                                                Join Now
                                            </i>
                                        </button>
                                        <div class="modal fade" id="Joinus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content" style="background-color: #2e323b; opacity: 1; border-radius: 10px">
                                                    <div class="modal-body">
                                                        <div class="modalforms">
                                                            <div class="modalforms-register">
                                                                <h4>REGISTRATION</h4>
                                                            </div>
                                                            <div class="modalforms-forms">
                                                                <form method="POST" >
                                                                    <div class="form-row">
                                                                        <div class="col">
                                                                            <label for="" class="labels">First Name</label>
                                                                            <input type="text" name="firstname" id="firstname" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                            <label for="" class="labels">Email Address</label>
                                                                            <input  name="email" id="email" type="email" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                            <label for="" class="labels">Password</label>
                                                                            <input  name="password" id="password" type="password" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                        </div>
                                                                        <div class="col">
                                                                            <label for="" class="labels">Last Name</label>
                                                                            <input  name="lastname" id="lastname" type="text" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                            <label for="" class="labels">Contact Number</label>
                                                                            <input  name="phonenumber" id="phonenumber" type="text" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                            <label for="" class="labels">Confirm Password</label>
                                                                            <input  type="password" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="modalform-submit">
                                                                        <button type="button" class="btn btn-secondary" id="signup">Continue</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="header-logins " id="thisss">
                    <div class="logins">
                        <div class="login-3welogo">
                            <img src="images/logo.svg" alt="">
                        </div>
                        <div class="login-username">
                            <a><i class="usersName"></i></a> 
                        </div>
                        <div class="login-datetime">
        
                        </div>
                        <div class="login-country"></div>
                        <div class="login-logout">
                            <a class="logout">
                                 <img class="ims" src="images/logout.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="header-country">
                    <!-- <div class="header-select">
                        <select class="form-select" aria-label="Default select example">
                            <option value="1">Singapore</option>
                            <option value="2">Malaysia</option>
                            <option value="3">Macau</option>
                        </select>
                    </div> -->
                    <!-- <div class="header-country-icon">
                        <img src="./images/singaporeFlag.png" alt="">
                    </div>
                    <div class="header-country-cname">
                        Singapore
                    </div> -->
                </div>
            </div>
        </div>
        <div class="qr">
            <div class="menu container">
                <div class="tab-menus menu-home">
                    <div class="tab-menus menu-home-icon">
                        <img src="./images/homeicon.png" alt="">
                    </div>
                    <div class="tab-menus menu-home-text schedzoom">
                        <a href="index.php">
                           <h6> Home </h6>
                        </a>
                    </div>
                </div>
                <div class="tab-menus menu-schedule  schedzoom">
                    <a href="./pages/schedule.php">
                        <h6>
                            Schedule
                        </h6>
                    </a>
                </div>
                <div class="tab-menus menu-livestream schedzoom">
                    <a href="./pages/livestream.php">
                        <h6>
                                Live Streaming
                        </h6>
                    </a>
                </div>
                <div class="tab-menus menu-horsebet schedzoom">
                    <a href="./pages/bettingtips.php">
                        <h6>
                            Horse Betting Tips
                        </h6>
                    </a>
                </div>
                <div class="tab-menus menu-horsecard schedzoom">
                        <a href="./pages/horseracecard.php">
                            <h6>
                                Horse Race Cards
                            </h6>
                        </a>
                </div>
                <div class="tab-menus menu-testimonial schedzoom">
                    <a href="./pages/testimonials.php">
                        <h6>
                            Testimonials
                        </h6>
                    </a>
                </div>
                <div class="tab-menus menu-blog schedzoom">
                    <a href="./pages/blogs.php">
                        <h6>
                            Blogs
                        </h6>
                    </a>
                </div>
                <div class="tab-menus menu-horseracingaccount schedzoom">
                    <a href="#">
                        <h6>
                            Horse Racing Account
                        </h6>
                    </a>
                </div>
                <div class="tab-menus menu-search">
                    <div class="form-group has-search" style="width: 110%">
                        <input type="text" class="form-control" placeholder="Search" style="background-color: #4b515f; color: white;">
                    </div>
                </div>
            </div>

        </div>
            <div class="wehorsecontent">
                <div class="wehorsecontent-content1">
                    <div class="webhorsecontent-carousel">
                   

                        <div id="carouselExampleIndicators" class="carousel slide carousel" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li style="background-color: #0759bf" data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                                <li style="background-color: #0759bf" data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                                <li style="background-color: #0759bf" data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
                            </ol>
                            <div class="carousel-inner carinner" style="max-height: 60vh;">
                                <div class="carousel-item active">
                                    <img src="./images/carousel1.png" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img src="./images/carousel2.png" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img src="./images/horse3.jpg" alt="">
                                </div>
                            </div>
                            <a class="carousel-control-prev" style="width: 0px" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only"><</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div style="background-image: linear-gradient(90deg, #09203F 0%, #537895 100%);   box-shadow: 0px 3px 2px 0px #000000 50%;">
                <div class="announcement container">
                    <div class="announcement-announceicon">
                        <img src="./images/announceicon.png" alt="">
                    </div>
                    <div class="announcement-announcetext">
                        <div id="display">
                            <p id="text" style="font-size: 14px; color: #F6ECA1;">
                                Welcome to 3WE Horse Racing Official Website. Do join our Telegram group chat for more tips and discussion. (@sg3wehorse)
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container mt-4">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <span style="font-weight: bold; text-shadow: 0px 3px 3px rgb(84 84 84 / 62%);">Latest Highlights</span>
                                    <!-- <small style="float: right"><a href="#">MORE VIDEOS</a></small> -->
                                </h4>
                            </div>
                            <div class="col-12">
                                <div class="wehorsecontent2-slide1-slide11">
                                    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                            <li data-target="#carouselExampleCaptions" data-slide-to="1" class=""></li>
                                            <li data-target="#carouselExampleCaptions" data-slide-to="2" class=""></li>
                                            <li data-target="#carouselExampleCaptions" data-slide-to="3" class=""></li>
                                            <li data-target="#carouselExampleCaptions" data-slide-to="4" class=""></li>
                                            <li data-target="#carouselExampleCaptions" data-slide-to="5" class=""></li>

                                        </ol>
                                        <div class="carousel-inner" style="padding-top: 5px; border-radius: 10px">
                                            <div class="carousel-item">
                                                <iframe width="100%" height="600"
                                                    src="https://www.youtube.com/embed/X5wXH6DCQho">
                                                </iframe>
                                                <div class="carousel-caption d-none d-md-block">
                                                </div>
                                            </div>
                                            <div class="carousel-item active">
                                                <iframe width="100%" height="600"
                                                    src="https://www.youtube.com/embed/c3pW7wHFB4Y">
                                                </iframe>
                                                <div class="carousel-caption d-none d-md-block">
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <iframe width="100%" height="600"
                                                    src="https://www.youtube.com/embed/QG4HD7sXGzU">
                                                </iframe>
                                                <div class="carousel-caption d-none d-md-block">
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <iframe width="100%" height="600"
                                                    src="https://www.youtube.com/embed/QG4HD7sXGzU">
                                                </iframe>
                                                <div class="carousel-caption d-none d-md-block">
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <iframe width="100%" height="600"
                                                    src="https://www.youtube.com/embed/QG4HD7sXGzU">
                                                </iframe>
                                                <div class="carousel-caption d-none d-md-block">
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <iframe width="100%" height="600"
                                                    src="https://www.youtube.com/embed/QG4HD7sXGzU">
                                                </iframe>
                                                <div class="carousel-caption d-none d-md-block">
                                                </div>
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <span style="font-weight: bold; text-shadow: 0px 3px 3px rgb(84 84 84 / 62%);"> Upcoming Race </span>
                                    <!-- <small style="float: right"><a href="#">MORE VIDEOS</a></small> -->
                                </h4>
                            </div>
                            <div class="col-12">
                                <div class="card" style="overflow-y: scroll; height: 600px">
                                    <?php
                                        include './data/tableschedule1.php';
                                        include './data/tableschedule2.php';
                                        include './data/tableschedule3.php';
                                        include './data/tableschedule4.php';
                                    ?>
                                   
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4 mb-1">
                    <div class="col-12">
                        <h4>
                            <span style="font-weight: bold; text-shadow: 0px 3px 3px rgb(84 84 84 / 62%);">Testimonials</span>
                        </h4>
                    </div>
                    <div id="testislides" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li style="background-color: black;" data-target="#testislides" data-slide-to="0" class="active"></li>
                            <li style="background-color: black;" data-target="#testislides" data-slide-to="1"></li>
                            <li style="background-color: black;" data-target="#testislides" data-slide-to="2"></li>
                            <li style="background-color: black;" data-target="#testislides" data-slide-to="3"></li>
                            <li style="background-color: black;" data-target="#testislides" data-slide-to="4"></li>
                            <li style="background-color: black;" data-target="#testislides" data-slide-to="5"></li>
                        </ol>
                        <div class="carousel-inner" style="padding-bottom: 60px">
                            <div class=" caroitem carousel-item active">
                                <a style="text-decoration: none; color: black" href="./pages/testimonials.php">
                                    <div class="card-body crdtesti ">
                                        <h5 class="card-title">
                                            "Had been playing with 3we for almost 1 year
                                            and I can say I'm already not planning
                                            to change a reliable agent.
                                            Keep up the good work!"
                                        </h5>
                                    </div>
                                </a>
                            </div>
                            <div class=" caroitem carousel-item ">
                                <a style="text-decoration: none; color: black" href="./pages/testimonials.php">
                                    <div class="card-body crdtesti">
                                        <h5 class="card-title">
                                        "Overall everything is alright, payout had not been delayed except if during peak but nevertheless, payout still done nicely."
                                        </h5>
                                    </div>
                                </a>
                            </div>
                            <div class=" caroitem carousel-item">
                                <a style="text-decoration: none; color: black" href="./pages/testimonials.php">
                                    <div class="card-body crdtesti">
                                        <h5 class="card-title">
                                        "Account do have live streaming which is a plus point. They even provide live streaming on facebook for those that do not own a account with them."
                                        </h5>
                                    </div>
                                </a>
                            </div>
                            <div class="caroitem carousel-item">
                                <a style="text-decoration: none; color: black" href="./pages/testimonials.php">
                                    <div class="card-body crdtesti">
                                        <h5 class="card-title">
                                            "Had been playing with 3we for almost 1 year
                                            and I can say I'm already not planning
                                            to change a reliable agent.
                                            Keep up the good work!"
                                        </h5>
                                    </div>
                                </a>
                            </div>
                            <div class="caroitem carousel-item">
                                <a style="text-decoration: none; color: black" href="./pages/testimonials.php">
                                    <div class="card-body crdtesti">
                                        <h5 class="card-title">
                                        "Overall everything is alright, payout had not been delayed except if during peak but nevertheless, payout still done nicely."
                                        </h5>
                                    </div>
                                </a>
                            </div>
                            <div class="caroitem carousel-item">
                                <a style="text-decoration: none; color: black" href="./pages/testimonials.php">
                                    <div class="card-body crdtesti">
                                        <h5 class="card-title">
                                        "Account do have live streaming which is a plus point. They even provide live streaming on facebook for those that do not own a account with them."
                                        </h5>
                                    </div>
                                </a>
                            </div>
                        </div>
                      
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-12">
                        <h4>
                            <span style="font-weight: bold; text-shadow: 0px 3px 3px rgb(84 84 84 / 62%);">Latest Horse News</span>
                            <!-- <small style="float: right"><a href="#">MORE VIDEOS</a></small> -->
                        </h4>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-12">
                        <div class="wehorsecontent3-latesthorsenews">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <img src="./images/horsenews1.png" alt="">
                                    </div>
                                    <div class="col">
                                        <h4>
                                            Clements celebrates title with treble
                                        </h4>
                                        <p>
                                            Michael Clements celebrated his first trainerâ€™s championship in Singapore in style at Kranji here today, the final of the 2020 racing season, with three winners.
                                            Already assured of the premiership before the dayâ€™s proceedings, Clements nevertheless wrapped up the year with wins from STARLIGHT, IMPERIUM and KNIGHT LOVE.
                                            STARLIGHT was a runaway winner in his debut while IMPERIUM was at his second win of the season, after scoring twice in 2018 but drawing a blank last year...

                                        </p>
                                        <h6>
                                            Read More...
                                        </h6>
                                    </div>
                                    <div class="w-100" style="margin: 10px"></div>
                                    <div class="col">
                                        <img src="./images/horsenews2.png" alt="">
                                    </div>
                                    <div class="col">
                                        <h4>
                                            Clements celebrates title with treble
                                        </h4>
                                        <p>
                                            Michael Clements celebrated his first trainerâ€™s championship in Singapore in style at Kranji here today, the final of the 2020 racing season, with three winners.
                                            Already assured of the premiership before the dayâ€™s proceedings, Clements nevertheless wrapped up the year with wins from STARLIGHT, IMPERIUM and KNIGHT LOVE.
                                            STARLIGHT was a runaway winner in his debut while IMPERIUM was at his second win of the season, after scoring twice in 2018 but drawing a blank last year...

                                        </p>
                                        <h6>
                                            Read More...
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
            <div class="icon-bar">
                <div class="holdingbox">
                <span class="rightbox">
                <span class="content">
                    <div class="iconss">
                    <div class="icons1">
                        <div class="icondiv">
                        <a class="fbzoom" href="https://www.facebook.com/3wehorse" class="facebook" target="_blank"><i class="fa fa-facebook"></i>
                            <h6>@3wehorse</h6>
                        </a> 
                        </div>
                    </div>
                    <div class="icons2">
                        <div class="icondiv">
                        <a class="tgzoom" href="https://t.me/sg3wehorse" class="telegram"  target="_blank"><i class="fa fa-telegram"></i>
                            <h6>@sg3wehorse</h6>
                        </a> 
                        </div>
                    </div>
                    <div class="icons3">
                        <div class="icondiv">
                        <a class="twtzoom" href="https://twitter.com/3wehorse1" class="twitter"  target="_blank"><i class="fa fa-twitter"></i>
                            <h6> @3wehorse1</h6>
                        </a> 
                        </div>
                    </div>
                    <div class="icons4">
                        <div class="icondiv">
                        <a class="ytzoom" href="https://www.youtube.com/channel/UCul4D-mYIICn8LzAQVeUNdQ" class="youtube"  target="_blank"><i class="fa fa-youtube"></i>
                            <h6>@3wehorse</h6>
                        </a> 

                        </div>
                    </div>
                    <div class="icons5">
                        <div class="icondiv">
                        <a class="w3zoom" href="https://www.3webet.com" class="webet"  target="_blank"><i><img src="images/3webet.png" alt="" style="width: 30px"></i>
                            <h6> 3webet.com</h6>
                        </a> 
                        </div>
                    </div>
                    <div class="icons6">
                        <div class="icondiv">
                        <a class="tikzoom" href="https://www.tiktok.com/@3wehorse" class="tiktok"  target="_blank"><i><img src="images/tiktok.png" alt="" style="width: 30px"></i>
                            <h6>@3wehorse</h6>
                        </a> 

                        </div>
                    </div>
                    <div class="icons7">
                        <div class="icondiv">
                        <a class="wezoom" href="https://weixin://dl/chat?x3webetadmin"  target="_blank"><i class="fa fa-weixin" aria-hidden="true"></i>
                        <h6>
                            x3webetadmin
                        </h6>
                        </a> 

                        </div>
                    </div>
                    <div class="icons8">
                        <div class="icondiv">
                        <a class="whatsappzoom" href="https://web.whatsapp.com/send?phone=6581348160&text=Hello"  target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i>
                            <h6 style="text-align: revert">SG: +65 8134 8160 <br> MY: +60 11-7263 2654 </h6>
                        </a>

                        </div>
                    </div>
                    </div>
                </span>
                </span>
                <span class="leftbox">
                <div class="stickys">
                    <p>contact us</p>
                    <i class="fa fa-phone"></i>
                </div>
                </span>
                </div>
                </div>

            <!-- <div class="icon-bar">
                <a id="minus" class="minus" style="border-radius: 30px; width:60px"><i class="fa fa-minus"></i></a> 
                <a id="hide" class="plus" style="border-radius: 30px; width:60px"><i class="fa fa-plus"></i></a> 
                <div class="ics"> 
                <a class="fbzoom" href="https://www.facebook.com/3wehorse" class="facebook" target="_blank"><i class="fa fa-facebook"></i></a> 
                <a class="tgzoom" href="https://t.me/sg3wehorse" class="telegram"  target="_blank"><i class="fa fa-telegram"></i></a> 
                <a class="twtzoom" href="https://twitter.com/3wehorse1" class="twitter"  target="_blank"><i class="fa fa-twitter"></i></a> 
                <a class="ytzoom" href="https://www.youtube.com/channel/UCul4D-mYIICn8LzAQVeUNdQ" class="youtube"  target="_blank"><i class="fa fa-youtube"></i></a> 
                <a class="w3zoom" href="https://www.3webet.com" class="webet"  target="_blank"><i><img src="images/3webet.png" alt="" style="width: 30px"></i></a> 
                <a class="tikzoom" href="https://www.tiktok.com/@3wehorse" class="tiktok"  target="_blank"><i><img src="images/tiktok.png" alt="" style="width: 30px"></i></a> 
                <a class="wezoom" href="https://weixin://dl/chat?x3webetadmin"  target="_blank"><i class="fa fa-weixin" aria-hidden="true"></i></a> 
                <a class="whatsappzoom" href="https://web.whatsapp.com/send?phone=6581348160&text=Hello"  target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
            </div> -->
            </div>
            </div>
            <div class="footer">
                <div class="footer-content">
                    <div class="footer-content-1">
                        <h3>About us</h3>
                        <h5>Online Horse Racing Betting Site in Singapore</h5>
                        <p>
                        Are you also show a big fan of horse racing in Singapore cannot make it there? We have a great news for you. We can help you in Singapore horse racing online betting. We are one of the best online horse betting sites you will ever come across. Horse racing betting online has become really popular nowadays but you don’t always find the authentic website for it. With us, you need not worry about it all. Horse betting in Singapore is really famous and is one of the most important reasons for some tourists visiting Singapore. Not everyone can afford to visit Singapore every now and then for horse racing betting, but this is now made possible by online horse racing sites Singapore. We are one of the best online horse betting sites because we not only provide the real feel of betting in the horse race but we also provide horse racing gambling Singapore. We also make sure that our clients receive the horse racing card Singapore which helps them get various discounts in our Singapore horse racing portal. Today’s horse racing betting Singapore has become immensely popular along with the horse racing sites Singapore which has enabled players across the world to play from the comfort of their home.
                        </p>
                    </div>
                    <div class="footer-content-2">
                        <div class="footer-faq">
                            <p>
                                <!-- <a href="./footerpages/faq.php">FAQ</a> |
                                <a href="./footerpages/begginerguide.php">Beginners Guide</a> |
                                <a href="./footerpages/rules.php">Rules</a> |  -->
                                <!-- <a href="./footerpages/responsiblegamblingpolicy.php">Responsible Gambling Policy</a> |
                                <a href="./footerpages/privacy.php">Privacy </a> |  -->
                                <!-- <a href="./footerpages/disclaimer.php">Disclaimer</a> | 
                                <a href="./footerpages/securitytips.php">Security Tips</a> -->
                            </p>
                    </div>
                        <div class="footer-copyright">
                            <p>
                                Copyright 2021 3wehorse.com | All Rights Reserved.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.holdingbox').hover(function(){
        $('.rightbox').stop().animate({width: '150px', height: '800px'}, 500)
    }, function(){
        $('.rightbox').stop().animate({width: '-0'}, 500)
  });
</script>

<script type="text/javascript">

    // $(document).ready(function () {
    //     $('.menu-home').on('click', function() {
    //     // $('button').removeClass('active');
    //     $(this).addClass('active');
    // });
    // });



    $(document).ready(function(){
        $('#signup').on('click', function(e){
            e.preventDefault();
            let firstname = $('#firstname').val()
            let lastname = $('#lastname').val()
            let phonenumber = $('#phonenumber').val()
            let email = $('#email').val()
            let password = $('#password').val()
                $.ajax({
                method:'POST',
                dataType: 'JSON',
                url:"http://localhost/3wehorse/api/register.php",
                data: JSON.stringify({ firstname, lastname, phonenumber, email, password}),
                }).done(function(response){
                    if(response.status == 201){
                        Swal.fire({
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: true,
                        }).then(function(){
                            window.location.href ='index.php'
                        })
                    }else{
                        Swal.fire({
                        icon: 'error',
                        title: response.message,
                        })
                    }
                    localStorage.setItem("name", firstname);
                });
            });
        })

    $(document).ready(function(){
        $('#loginme').on('click', function(e){
            e.preventDefault();
            let email = $('#emaillogin').val()
            let password = $('#passwordlogin').val()
                $.ajax({
                    method:'POST',
                    dataType: 'JSON',
                    url:"http://localhost/3wehorse/api/login.php",
                    data: JSON.stringify({email, password, firstname}),
                    }).done(function(response){
                    if(response.success == 1){
                        Swal.fire({
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: true,
                        }).then(function(){
                            window.location.href ='index.php'
                        })
                    }
                    else{
                        Swal.fire({
                        icon: 'error',
                        title: response.message,
                        })
                    }
                    console.log("OIOIOOOIOIOIOIOI", firstname)

                    localStorage.setItem("user", email);
                    localStorage.setItem("token", response.token);
                   


                    if(response.success == 0){
                        localStorage.clear();
                    }else{
                    }

                });
            });
            
        })

        // $(document).ready(function(){
        //     $.ajax({
        //         method:'GET',
        //         dataType: 'JSON',
        //         url:"http://localhost/3wehorse/api/userinfo.php",
        //         headers: {
        //             "Authorization": "Bearer " + userToken
        //         },
        //         }).done(function(response){
        //         console.log("newwwwwwww",response)
        //         console.log("firstNAme", response.user.firstname)
        //         console.log("wwwwwwwwwwwwin", userToken)
        //         $('.usersName').html(response.user.firstname);
        //     });
        // })
    
        $(document).ready(function(){
            var retrievedObject = localStorage.getItem('name');

            var userLogin = localStorage.getItem('user');
            var userToken = localStorage.getItem('token');
            console.log("++++++++++++++++++++++++++++++++++++", retrievedObject);

            if(userLogin == null){
                console.log("no user logged in")
                $('.header-login').show();
                $('#thisss').hide();
            }else if(userToken == null){
                console.log("no yser")
                $('.header-login').show();
                $('#thisss').hide();
            }else if(userLogin !=null){
                $('.header-logins').hide();
                $('#thisss').show();
                console.log("hello ")
            }else if(userToken !=null){
                $('.header-logins').hide();
                $('#thisss').show();
                console.log("hello asdasdsd")
            }
    
            $.ajax({
                method:'GET',
                dataType: 'JSON',
                url:"http://localhost/3wehorse/api/userinfo.php",
                headers: {
                    "Authorization": "Bearer " + userToken
                },
                }).done(function(response){
                // console.log(">>>>>>>>>>>",response)
                // console.log("firstNAme", response.user.firstname)
                // console.log(">>>>>>>>>>>++++++++++++++", userToken)
                $('.usersName').html(response.user.firstname);
            });

            $(".logout").click(function(){
                location.reload();
                localStorage.clear();
            });

        })

    function marquee(a, b) {
    var width = b.width();
    var start_pos = a.width();
    var end_pos = -width;

        function scroll() {
            if (b.position().left <= -width) {
                b.css('left', start_pos);
                scroll();
            }
            else {
                time = (parseInt(b.position().left, 10) - end_pos) *
                (40000/ (start_pos - end_pos)); // Increase or decrease speed by changing value 10000
                b.animate({
                'left': -width
                }, time, 'linear', function() {
                scroll();
                });
                }
        }

            b.css({
            'width': width,
            'left': start_pos
            });
            scroll(a, b);

            b.mouseenter(function() {     // Remove these lines
            b.stop();                 //
            b.clearQueue();           // if you don't want
            });                           //
            b.mouseleave(function() {     // marquee to pause
            scroll(a, b);             //
            });                           // on mouse over

        }

            $(document).ready(function() {
            marquee($('#display'), $('#text'));  //Enter name of container element & marquee element
    });

    $( "#start" ).on( "keydown", function(event) {
      if(event.which == 13) 
         alert("Entered!");
    });
</script>
<script>
    fetch("https://www.bloodhorse.com/horse-racing/feeds/news/todays-headlines")
        .then( response => response.text() )
        .then(str => new window.DOMParser().parseFromString(str, "text/xml"))
        .then(data => {
            const items = data.querySelectorAll("item")
            let ctr = 0
            let sportsWidget = ""
            items.forEach(item => {
                let title = $($(item).find("title")[0])[0].textContent
                let description = $($(item).find("description")[0])[0].textContent
                let image = $($(item).find("image")[0])[0]
                let link = $($(item).find("link")[0])[0].textContent
                let date = $($(item).find("pubDate")[0])[0].textContent

                if (image) {
                sportsWidget += "<div class=\"row\" style=\"margin-bottom: 10px; background: #1c1b1b; padding-top: 15px;\"><div class=\"col-md-4\" >"
                    sportsWidget += "<img src=\""+image.textContent+"\" class=\"feed-image\" style=\"width: 100%;\"></div>"
                sportsWidget += "<div class=\"col-md-8\">"
                    sportsWidget += "<h6 class=\"title \">"+title+"</h6>"
                    sportsWidget += "<p class=\"mute\" style=\"color: #7d7d7d\"><small>"+date+"</small></p>"
                    sportsWidget += "<p class=\"title mt-2\" style=\"color: #7d7d7d; white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 200ch;\">"+description+"</p>" 
                    sportsWidget += "<p class=\"title mt-2\"><a href=\""+link+"\" style=\"font-weight: bold;\" target=\"_blank\">Read more</a></p>"
                    sportsWidget += "</div></div>"
                
                    // increase counter
                    ctr++
                }
                
                if (ctr == 4) { $("#sportsWidget").html(sportsWidget) }
            })
        })
</script>
<script>
$(document).ready(function(){
  var rid = $("#hide");
  $(".ics").hide();
  $(".minus").hide();
  $("#hide").click(function(){
    $(".ics").show();
    $(".minus").show();
    $(".plus").hide();
  });

});

  $(document).ready(function($){
    $("#minus").click(function(){
    $('.ics').hide();
    $(".plus").show();
    $(".minus").hide();
  })
  })
</script>
<?php include 'scripts/tawktoscript.php'?>
</body>
</html>