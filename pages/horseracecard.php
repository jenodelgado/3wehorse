<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/cssGrid.css">
    <link rel="stylesheet" href="../css/mediaQuery.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbars/">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <title>Horse Race Card</title>
</head>
<body>

        <?php include '../corepage/header.php'?>
        <?php include '../corepage/menus.php'?>
        <div class="wehorsecontent">
            <div class="wehorsehorsecard container">
                <div class="scrollable">
                    <div class="row">
                        <div class="col-sm">
                            <div class="wehorsecard-sa1">
                                    <img src="../images/hongkongrc.png" alt="">
                                </div>
                                <div class="wehorsecard-sa2">
                                    <a href="hongkongracecard.php">
                                        <div  class="sa2 imzoom">
                                            <h4>HONG KONG RACE CARD</h4>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <div class="col-sm">
                            <div class="wehorsecard-s2-1">
                                <img src="../images/singaporerc.png" alt="">
                            </div>
                            <div class="wehorsecard-s2-2">
                                <a href="singaporeracecard.php">
                                    <div class="wc imzoom">
                                        <h4>SINGAPORE RACE CARD</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <div class="wehorsecard-s3-1">
                                <img src="../images/malaysiarc.png" alt="">
                            </div>
                            <div class="wehorsecard-s3-2">
                                <a href="malaysiaracecard.php">
                                    <div class="wc imzoom">
                                        <h4>MALAYSIA RACE CARD</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="wehorsecard-s4-1">
                                <img src="../images/macaurc.png" alt="">
                            </div>
                            <div class="wehorsecard-s4-2">
                                <a href="macauracecard.php">
                                    <div class="wc imzoom">
                                        <h4>MACAU RACE CARD</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-sm">
                            <div class="wehorsecard-s3-1">
                                <img src="../images/malaysiarc.png" alt="">
                            </div>
                            <div class="wehorsecard-s3-2">
                                <a href="">
                                    <div class="wc imzoom">
                                        <h4>MALAYSIA RACE CARD</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="wehorsecard-s4-1">
                                <img src="../images/macaurc.png" alt="">
                            </div>
                            <div class="wehorsecard-s4-2">
                                <a href="">
                                    <div class="wc imzoom">
                                        <h4>MACAU RACE CARD</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div> -->
                </div>
                    
            </div>
        </div>
        <?php include '../corepage/stickyicons.php'?>

        <?php include '../corepage/footer.php'?>
    </div>
</body>
<?php include '../scripts/scripts.php'?>
<?php include '../scripts/tawktoscript.php'?>
</html>