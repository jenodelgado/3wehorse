<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/cssGrid.css">
    <link rel="stylesheet" href="../css/mediaQuery.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbars/">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="AjaxPush.js"></script>
    
    <title>Livestream</title>
</head>
<body>

<div class="wrapper">
<?php include '../corepage/header.php'?>
<?php include '../corepage/menus.php'?>  
<div class="hiddenpopup" >
    <button type="button"  style="display:none" class="btn btn-primary popupme" data-toggle="modal" data-target="#popup">
        Launch demo modal
    </button>

    <div class="modal fade" id="popup" tabindex="-1" data-backdrop="static" data-keyboard="false"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <!-- <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div> -->
        <div class="modal-body">
            <p>To continue watching log in first</p>
            <a href="../index.php"> CLick Me</a>

        </div>
        <!-- <div class="modal-footer">
            <button type="button"  class="btn btn-secondary " data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div> -->
        </div>
    </div>
    </div>
</div>
    
            <div class="wehorsecontent">
                <div class="wehorselivestream container">
                    <div class="wehorselivestream-livefeed">
                        <div>
                            <ul class="nav nav-tabs">
                                <li><a data-toggle="tab" id="channel1" href="#chanel1">Singapore</a></li>
                                <li><a data-toggle="tab" id="channel2" href="#chanel2">Malaysia</a></li>
                                <li><a data-toggle="tab" id="channel3" href="#chanel3">Macau </a></li>
                                <li><a data-toggle="tab" id="channel4" href="#chanel4">Hong Kong</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="chanel1" class="tab-pane fade in active">
                                    <?php include '../livestreamlinks/singapore.php'?>
                                </div>
                                <div id="chanel2" class="tab-pane fade">
                                    <?php include '../livestreamlinks/hongkong1.php'?>
                                </div>
                                <div id="chanel3" class="tab-pane fade">
                                    <?php include '../livestreamlinks/macau.php'?>
                                </div>
                                <div id="chanel4" class="tab-pane fade">
                                    <?php include '../livestreamlinks/hongkon2.php'?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wehorselivestream-chatbox">
                        <div>
                            <div class="wrapperss">
                                <div class="chattitle">
                                    <h2>Chat Room</h2>
                                </div>
                                <div class="chat-content" id="history"></div>
                                <div class="ctrl">
                                    <div class="ctrlarea">
                                        <input class="form-control inputss" type="text" autofocus id="message" placeholder="your message!">
                                    </div>
                                    <div class="ctrlsend">
                                        <img onclick="send()" id="snd" style="width:80px; padding-top: 10px;" src="../images/paperplane.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="wehorselivestreamlist-list">
                        <div class="wehorselivestreamlist-lista">
                            <div class="card" >
                                <div class="card-header lvstrm" style="background-color: #3c4852">SINGAPORE KRANJI</div>
                                <div class="card-body">
                                    <img id="ch1"style="width:100%" src="../images/livestream1.png" alt="">
                                        <a href="#"style="text-align: center">
                                            <p>
                                                Channel 1
                                            </p>    
                                        </a>    
                                </div>
                            </div>
                        </div>
                        <div class="wehorselivestreamlist-listb">
                            <div class="card">
                                <div class="card-header lvstrm" style="background-color: #3c4852">MALAYSIA</div>
                                <div class="card-body">
                                    <img id="ch2" style="width:100%" src="../images/livestream1.png" alt="">
                                        <a href="#"  style="text-align: center">
                                            <p>
                                                Channel 2
                                            </p>    
                                        </a>   
                                </div>
                            </div>
                        </div>
                        <div class="wehorselivestreamlist-listc">
                            <div class="card">
                                <div class="card-header lvstrm" style="background-color: #3c4852">MACAU TAIPA</div>
                                <div class="card-body">
                                    <img id="ch3" style="width:100%" src="../images/livestream1.png" alt="">  
                                        <a href="#"  style="text-align: center">
                                            <p>
                                                Channel 3
                                            </p>    
                                        </a>     
                                </div>
                            </div>
                        </div>
                        <div class="wehorselivestreamlist-listd">
                            <div class="card">
                                <div class="card-header lvstrm" style="background-color: #3c4852">HONG KONG</div>
                                <div class="card-body">
                                    <img id="ch4" style="width:100%" src="../images/livestream1.png" alt="">
                                        <a href="#"  style="text-align: center">
                                            <p>
                                                Channel 4
                                            </p>    
                                        </a>   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wehorselivestreamchannel-list">
                        <div class="chanllist">
                        </div>
        
                    </div>
                </div>
            </div>
            <?php include '../corepage/stickyicons.php'?>
            <?php include '../corepage/footer.php'?>
    </div>
</body>
<script>
    $(document).ready(function(){
        $("#ch1").click(function(){
            $("#channel1").click(); 
            return false;
        });
        $("#ch2").click(function(){
            $("#channel2").click(); 
            return false;
        });
        $("#ch3").click(function(){
            $("#channel3").click(); 
            return false;
        });
        $("#ch4").click(function(){
            $("#channel4").click(); 
            return false;
        });
    })
</script>
<script>
     var userLogin = localStorage.getItem('user')
    $(document).ready(function(){
        if(userLogin == null){
            setTimeout(function() {
                $(".popupme").trigger('click');

                $('.hiddenpopup').show();
            },60000);
        }else{

        }
    })

</script>
<script>
    $(document).keypress(function(event) {
            if (event.key === "Enter") {
                $('#snd').trigger('click');
            }
        });
</script>

<script type="text/javascript">

    var userLogin = localStorage.getItem('user')
    var userName = localStorage.getItem('token')
    var comet = new AjaxPush('listener.php', 'sender.php');
    var n = new Function("return (Math.random()*190).toFixed(0)");
   

    // var userName = localStorage.getItem('name')
    console.log("99999999999999999999", userLogin);
    // create anonymous users
    // var c = "rgb(" + userLogin + ")";
    var c = "rgb(" + n() + ", " + n() + "," + n() + ")";
    var template = "<strong style='color: " + c + "'>" + 'user_)' + n() + "</strong>: ";
    console.log("44444444444444", c);
    console.log("111111111111111111111", template);
    // listener
    comet.connect(function(data) { $("#history").append(data.message) + "<br>"; });

    // sender
    var send = function() {
        if(userLogin == null){
            Swal.fire({
                icon: 'error',
                title: 'You need to Logged In First!',
            })
        }
        else{
            comet.doRequest({ message: userLogin +" <span></span>"+ $("#message").val() + "<br>" }, function(){
                $("#message").val('').focus();
            })
        }
    }
</script>

<?php include '../scripts/scripts.php'?>
<?php include '../scripts/tawktoscript.php'?>
</html>