<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/cssGrid.css">
    <link rel="stylesheet" href="../css/mediaQuery.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbars/">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <title>Singapore Race Card</title>
</head>
<style>
    
* {
  box-sizing: border-box;
}

img {
  vertical-align: middle;
}

/* Position the image container (needed to position the left and right arrows) */
.container {
  position: relative;
}

/* Hide the images by default */
.mySlides {
  display: none;
}

/* Add a pointer when hovering over the thumbnail images */
.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 40%;
  width: auto;
  padding: 16px;
  /* margin-top: -50px; */
  color: white;
  font-weight: bold;
  font-size: 100px;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
  color: transparent;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: none;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* Container for image text */
.caption-container {
  text-align: center;
  background-color: #222;
  padding: 2px 16px;
  color: white;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Six columns side by side */
.column {
  float: left;
  width: 16.66%;
}

/* Add a transparency effect for thumnbail images */
.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

.imgss{
  width: 100%;
}

.imzoom{
        /* height: 250px; */
    }
    .imzoom:hover {
        -ms-transform: scale(1.2); 
        -webkit-transform: scale(1.2);
        transform: scale(1.2); 
    }

.left{
  font-size: 100px; 
  color: black; 
  margin-bottom: 229px;
}
  .left:hover{
    color: black;
  }

  .right{
  font-size: 100px; 
  color: black; 
  margin-bottom: 229px;
}
  .right:hover{
    color: black;
  }
</style>
<body>

        <?php include '../corepage/header.php'?>
        <?php include '../corepage/menus.php'?>
        <div class="wehorsecontent">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
          <div class="carousel-inner">
              <div class="carousel-item active">
                    <div class="container">
                      <h2 style="text-align:center">Singapore Horse Card Chinese</h2>
                      <div class="container">
                        <div class="mySlides">
                            <img  class="imgss" src="../images/s1.png" alt="">
                        </div>

                        <div class="mySlides">
                            <img class="imgss" src="../images/s2.png" alt="">
                        </div>

                        <div class="mySlides">
                            <img class="imgss" src="../images/s3.png" alt="">
                        </div>
                            
                        <div class="mySlides">
                            <img class="imgss" src="../images/s4.png" alt="">
                        </div>

                        <div class="mySlides">
                            <img class="imgss" src="../images/s5.png" alt="">
                        </div>
                            
                        <div class="mySlides">
                            <img class="imgss" src="../images/s6.png" alt="">
                        </div>
                        <div class="mySlides">
                            <img class="imgss" src="../images/s7.png" alt="">
                        </div>
                        <div class="mySlides">
                            <img class="imgss" src="../images/s8.png" alt="">
                        </div>
                        <div class="mySlides">
                            <img class="imgss" src="../images/s9.png" alt="">
                        </div>
                        <div class="mySlides">
                            <img class="imgss" src="../images/s10.png" alt="">
                        </div>
                        <a class="prev" onclick="plusSlides(-1)">❮</a>
                        <a class="next" onclick="plusSlides(1)">❯</a>

                      </div>
                      <div class="container" style="padding-top: 3%; padding-bottom: 3%;">
                      <div class=>
                        <div class="row">
                          <div class="col">
                            <img class="demo cursor imzoom" src="../images/s1.png" style="width:100%" onclick="currentSlide(1)" alt="The Woods">
                          </div>
                          <div class="col">
                          <img class="demo cursor imzoom" src="../images/s2.png" style="width:100%" onclick="currentSlide(2)" alt="Cinque Terre">

                          </div>
                          <div class="col">
                          <img class="demo cursor imzoom" src="../images/s3.png" style="width:100%" onclick="currentSlide(3)" alt="Mountains and fjords">
                          </div>
                          <div class="col">
                          <img class="demo cursor imzoom" src="../images/s4.png" style="width:100%" onclick="currentSlide(4)" alt="Northern Lights">
                          </div>
                          <div class="col">
                          <img class="demo cursor imzoom" src="../images/s5.png" style="width:100%" onclick="currentSlide(5)" alt="Nature and sunrise">
                          </div>
                          <div class="col">
                          <img class="demo cursor imzoom" src="../images/s6.png" style="width:100%" onclick="currentSlide(6)" alt="Snowy Mountains">
                          </div>
                          <div class="col">
                          <img class="demo cursor imzoom" src="../images/s7.png" style="width:100%" onclick="currentSlide(7)" alt="Snowy Mountains">
                          </div>
                          <div class="col">
                          <img class="demo cursor imzoom" src="../images/s8.png" style="width:100%" onclick="currentSlide(8)" alt="Snowy Mountains">
                          </div>
                          <div class="col">
                          <img class="demo cursor imzoom" src="../images/s9.png" style="width:100%" onclick="currentSlide(9)" alt="Snowy Mountains">
                          </div>
                          <div class="col">
                          <img class="demo cursor imzoom" src="../images/s10.png" style="width:100%" onclick="currentSlide(10)" alt="Snowy Mountains">
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              <div class="carousel-item">
              <div class="container">
                <h2 style="text-align:center">Singapore Horse Card English</h2>
                <div class="container">
                  <div class="mySlides2">
                      <img  class="imgss" src="../images/ss1.png" alt="">
                  </div>

                  <div class="mySlides2">
                      <img class="imgss" src="../images/ss2.png" alt="">
                  </div>

                  <div class="mySlides2">
                      <img class="imgss" src="../images/ss3.png" alt="">
                  </div>
                      
                  <div class="mySlides2">
                      <img class="imgss" src="../images/ss4.png" alt="">
                  </div>

                  <div class="mySlides2">
                      <img class="imgss" src="../images/ss5.png" alt="">
                  </div>
                      
                  <div class="mySlides2">
                      <img class="imgss" src="../images/ss6.png" alt="">
                  </div>
                  <div class="mySlides2">
                      <img class="imgss" src="../images/ss7.png" alt="">
                  </div>
                  <div class="mySlides2">
                      <img class="imgss" src="../images/ss8.png" alt="">
                  </div>
                  <div class="mySlides2">
                      <img class="imgss" src="../images/ss9.png" alt="">
                  </div>
                  <div class="mySlides2">
                      <img class="imgss" src="../images/ss10.png" alt="">
                  </div>
                  <a class="prev" onclick="plusSlidess(-1)">❮</a>
                  <a class="next" onclick="plusSlidess(1)">❯</a>

                </div>
                <div class="container" style="padding-top: 3%; padding-bottom: 3%;">
                <div class=>
                  <div class="row">
                    <div class="col">
                      <img class="demo cursor imzoom" src="../images/ss1.png" style="width:100%" onclick="currentSlide2(1)" alt="The Woods">
                    </div>
                    <div class="col">
                    <img class="demo cursor imzoom" src="../images/ss2.png" style="width:100%" onclick="currentSlide2(2)" alt="Cinque Terre">

                    </div>
                    <div class="col">
                    <img class="demo cursor imzoom" src="../images/ss3.png" style="width:100%" onclick="currentSlide2(3)" alt="Mountains and fjords">
                    </div>
                    <div class="col">
                    <img class="demo cursor imzoom" src="../images/ss4.png" style="width:100%" onclick="currentSlide2(4)" alt="Northern Lights">
                    </div>
                    <div class="col">
                    <img class="demo cursor imzoom" src="../images/ss5.png" style="width:100%" onclick="currentSlide2(5)" alt="Nature and sunrise">
                    </div>
                    <div class="col">
                    <img class="demo cursor imzoom" src="../images/ss6.png" style="width:100%" onclick="currentSlide2(6)" alt="Snowy Mountains">
                    </div>
                    <div class="col">
                    <img class="demo cursor imzoom" src="../images/ss7.png" style="width:100%" onclick="currentSlide2(7)" alt="Snowy Mountains">
                    </div>
                    <div class="col">
                    <img class="demo cursor imzoom" src="../images/ss8.png" style="width:100%" onclick="currentSlide2(8)" alt="Snowy Mountains">
                    </div>
                    <div class="col">
                    <img class="demo cursor imzoom" src="../images/ss9.png" style="width:100%" onclick="currentSlide2(9)" alt="Snowy Mountains">
                    </div>
                    <div class="col">
                    <img class="demo cursor imzoom" src="../images/ss10.png" style="width:100%" onclick="currentSlide2(10)" alt="Snowy Mountains">
                    </div>
                  </div>
                </div>
              </div>
          </div>
              </div>
            </div>
            <a class="carousel-control-prev" style="width:0" href="#carouselExampleControls" role="button" data-slide="prev">
              <p class="carousel-control-prev-icon left" aria-hidden="true" ><i class="fa fa-chevron-left"></i></p>
              <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> -->
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <p class="carousel-control-prev-icon right"  aria-hidden="true"><i class="fa fa-chevron-right"></i></p>

              <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span> -->
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
        <?php include '../corepage/stickyicons.php'?>

        <?php include '../corepage/footer.php'?>
    </div>
</body>
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
<script>
var slideIndex = 1;
showSlides2(slideIndex);

function plusSlidess(n) {
  showSlides2(slideIndex += n);
}

function currentSlide2(n) {
  showSlides2(slideIndex = n);
}

function showSlides2(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides2");
  var dots = document.getElementsByClassName("demo2");
  var captionText = document.getElementById("caption2");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
<?php include '../scripts/scripts.php'?>
<?php include '../scripts/tawktoscript.php'?>
</html>