<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/cssGrid.css">
    <link rel="stylesheet" href="../css/mediaQuery.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbars/">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="../calendar/fullcalendar/fullcalendar.min.css" />
    <script src="../calendar/fullcalendar/lib/jquery.min.js"></script>
    <script src="../calendar/fullcalendar/lib/moment.min.js"></script>
    <script src="../calendar/fullcalendar/fullcalendar.min.js"></script>
    <title>Schedule</title>
</head>
<body>
    <div class="wrapper">
    <?php include '../corepage/header.php'?>
    <?php include '../corepage/menus.php'?>
            <div class="wehorsecontent">
                <script>
                $(document).ready(function () {
                    var userLogin = localStorage.getItem('user')
                    var userToken = localStorage.getItem('token')
                    
                    var calendar = $('#calendar').fullCalendar({
                        editable: true,
                        events: "../calendar/fetch-event.php",
                        displayEventTime: false,
                        eventRender: function (event, element, view) {
                            if (event.allDay === 'true') {
                                event.allDay = true;
                            } else {
                                event.allDay = false;
                            }
                        }
                      
                    });
                });
                    function displayMessage(message) {
                            $(".response").html("<div class='success'>"+message+"</div>");
                        setInterval(function() { $(".success").fadeOut(); }, 1000);
                    }
                </script>
                <div class="h22">
                    <h2>Schedule Race and Upcoming Race</h2>
                </div>
                 <div class="response"></div>
                 <div id='calendar'></div>
                <!-- <div class="weschedule">
                    <div class="weschedule-img">
                    </div>
                    <div class="weschedule-img2">
                        <div class="card">
                            <div class="card-body">
                                <div class="a b c">
                                    <div class="weschedule-container-head container">
                                        <div class="weschedule-container-head-month">
                                            <h1> March 2021</h1>
                                        </div>
                                        <div class="weschedule-container-head-list" data-toggle="modal" data-target="#modallist">
                                            <h4> List</h4>
                                        </div>
                                        <div class="weschedule-container-head-lm">
                                            <h4> Month</h4>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="modallist" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" style="width:1250px" role="document">
                                            <div class="modal-content ">
                                                <div class="modal-body ">
                                                    <div class="card">
                                                        <ul class="list-group list-group-flush">
                                                            <li class="list-group-item">
                                                                <img style="width:100%" src="../images/list1.png" alt="">
                                                            </li>
                                                            <li class="list-group-item">
                                                                <img style="width:100%" src="../images/list2.png" alt="">
                                                            </li>
                                                            <li class="list-group-item">
                                                                <img style="width:100%" src="../images/list3.png" alt="">
                                                            </li>
                                                            <li class="list-group-item">
                                                                <img style="width:100%" src="../images/list4.png" alt="">
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" container weschedule-container" style="background-color: #ecf2f3;">
                                        <div class="weschedule-container-a ">
                                            <img class="schedzoom" src="../images/schedimg1.png" alt="">
                                        </div>
                                        <div class="weschedule-container-b">
                                            <img class="schedzoom" src="../images/schedimg1.png" alt="">
                                        </div>
                                        <div class="weschedule-container-c">
                                            <img class="schedzoom" src="../images/schedimg3.png" alt="">
                                        </div>
                                        <div class="weschedule-container-d">
                                            <img class="schedzoom" src="../images/schedimg4.png" alt="">
                                        </div>
                                        <div class="weschedule-container-e">
                                            <img class="schedzoom" src="../images/schedimg5.png" alt="">
                                        </div>
                                        <div class="weschedule-container-f">
                                            <img class="schedzoom" src="../images/schedimg6.png" alt="">
                                        </div>
                                        <div class="weschedule-container-g">
                                            <img class="schedzoom" src="../images/schedimg7.png" alt="">
                                        </div>
                                        <div class="weschedule-container-h">
                                            <img class="schedzoom" src="../images/schedimg8.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    </div>
                </div> -->
            </div>
            <?php include '../corepage/stickyicons.php'?>
            <?php include '../corepage/footer.php'?>
            <?php include '../scripts/tawktoscript.php'?>
    </div>
</body>
<?php include '../scripts/scripts.php'?>
</html>