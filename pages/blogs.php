<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/cssGrid.css">
    <link rel="stylesheet" href="../css/mediaQuery.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbars/">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <title>Livestream</title>
</head>
<body>
<div class="wrapper">
    <?php include '../corepage/header.php'?>
    <?php include '../corepage/menus.php'?>
        </div>
            <div class="wehorsecontent">
                <div class="wehorseblog container">
                    <div class="wehorseblog-container">
                        <div class="wehorseblog-blog1">
                            <div class="b1">
                                <img src="../images/blogimg1.png" alt="">
                            </div>
                            <div class="b2">
                                <h3>
                                    Veteran Banner looks to fly for Yiu in Sunday’s
                                    G1 Centenary Sprint Cup
                                </h3>
                                <p>
                                    Jolly Banner might be in the twilight of his career as a nine-year-old but that didn’t stop him from snaring second in last month’s G1 LONGINES Hong Kong Sprint (1200m) against an international field and his recent form has trainer Ricky Yiu brimming with optimism ahead of Sunday’s (24 January) G1 Centenary Sprint Cup (1200m).
                                </p>
                                <div class="toglep schedzoom" data-toggle="modal" data-target="#exampleModalLong">
                                    Readmore...
                                </div>
                            </div>
                            <div class="modal fade " id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog modal-xl mw-100 w-75"  role="document">
                                    <div class="modal-content" style="background-color: transparent">
                                        <div class="modal-body ">
                                            <div class="modalimg">
                                                <img src="../images/blog1.png" style="width: 100%" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wehorseblog-blog2">
                            <div class="bl1">
                                <h3>
                                    Jockeying for position as four-year-old
                                    series takes shape
                                </h3>
                                <p>
                                    Winning Dreamer rules himself out of the HK$12 million Classic Mile and ky Darci confirms he will be one of the leading contenders
                                    The Classic Mile is less than three weeks away and the New Year’s Day races helped answer some key questions ahead of the start of the four-year-old series – but it’s more about who isn’t chasing the huge prize money on offer.
                                </p>
                                <div class="toglep  schedzoom" data-toggle="modal" data-target="#exampleModalLong2">
                                    Readmore...
                                </div>
                            </div>
                            <div class="bl2">
                                <img src="../images/blogimg2.png" alt="">
                            </div>
                            <div class="modal fade" id="exampleModalLong2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog modal-xl mw-100 w-75"  role="document">
                                    <div class="modal-content" style="background-color: transparent">
                                        <div class="modal-body">
                                            <img src="../images/blog2.png" style="width: 100%" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wehorseblog-blog3">
                            <div class="blo1">
                                <img src="../images/blogimg3.png" alt="">
                            </div>
                            <div class="blo2">
                                <h3>
                                    Stipendiary Stewards Inquiry – Jockey F Henrique
                                </h3>
                                <p>
                                    RACE-1 THE LYON RATING 45-0 (CLASS 4&5) 1200 METRES (101)
                                    RACING INCIDENT & INQUIRY:
                                    The Stewards inquired into an incident near the 350 metres where 7 On Purpose, 11 Deep Brother and 6 At Ease had be checked. Evidence was taken from the riders of 4 Express Pony (D Barros), 5 Mulan Go (H Lam), 6 At Ease (M Tanaka), 7 On Purpose (S Nakano), 10 Sheng Li Star (F Henrique) and 11 Deep Brother (C L Cheung).
                                </p>
                                <div class="toglep schedzoom" data-toggle="modal" data-target="#exampleModalLong3">
                                    Readmore...
                                </div>
                            </div>
                            <div class="modal fade" id="exampleModalLong3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog modal-xl mw-100 w-75"  role="document">
                                    <div class="modal-content" style="background-color: transparent">
                                        <div class="modal-body">
                                            <img src="../images/blog3.png" style="width: 100%" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wehorseblog-blog4">
                            <div class="blog1">
                                <h3>
                                    Clements celebrates title with treble
                                </h3>
                                <p>
                                    Michael Clements celebrated his first trainer’s championship in Singapore in style at Kranji here today, the final of the 2020 racing season, with three winners.
                                    Already assured of the premiership before the day’s proceedings, Clements nevertheless wrapped up the year with wins from STARLIGHT, IMPERIUM and KNIGHT LOVE.
                                </p>
                                <div class="toglep schedzoom" data-toggle="modal" data-target="#exampleModalLong4">
                                    Readmore...
                                </div>
                            </div>
                            <div class="blog2">
                                <img src="../images/blogimg4.png" alt="">
                            </div>
                            <div class="modal fade" id="exampleModalLong4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog modal-xl  mw-100 w-75"  role="document">
                                    <div class="modal-content " style="background-color: transparent">
                                        <div class="modal-body">
                                            <img src="../images/blog4.png" style="width: 100%" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wehorseblog-blog5">
                            <div class="blog-bb1">
                                <img src="../images/blogimg5.png" alt="">
                            </div>
                            <div class="blog-bb2">
                                <h3>
                                    New champion trainer keeps show going with
                                    Mortal Engine
                                </h3>
                                <p>
                                Freshly-minted Singapore champion trainer Michael Clements picked up where he left off with already a first 2021 winner on the board in newcomer Mortal Engine at the inaugural season on Sunday.”
                                </p>
                                <div class="toglep schedzoom" data-toggle="modal" data-target="#exampleModalLong5">
                                    Readmore...
                                </div>
                            </div>
                            <div class="modal fade" id="exampleModalLong5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog modal-xl mw-100 w-75"  role="document">
                                    <div class="modal-content" style="background-color: transparent">
                                        <div class="modal-body">
                                            <img src="../images/blog5.png" style="width: 100%" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wehorseblog-blog6">
                            <div class="blog-ll1">
                                <h3>
                                    New champion trainer keeps show going with
                                    Mortal Engine
                                </h3>
                                <p>
                                Freshly-minted Singapore champion trainer Michael Clements picked up where he left off with already a first 2021 winner on the board in newcomer Mortal Engine at the inaugural season on Sunday.”
                                </p>
                                <div class="toglep schedzoom" data-toggle="modal" data-target="#exampleModalLong6">
                                    Readmore...
                                </div>
                            </div>
                            <div class="blog-ll2">
                                <img src="../images/blogimg5.png" alt="">
                            </div>
                            <div class="modal fade" id="exampleModalLong6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog modal-xl  mw-100 w-75"  role="document">
                                    <div class="modal-content" style="background-color: transparent">
                                        <div class="modal-body">
                                            <img src="../images/blog6.png" style="width: 100%" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wehorseblog-blog7">
                            <div class="blog-gg1">
                                <img src="../images/blogimg7.png" alt="">
                            </div>
                            <div class="blog-gg2">
                                <h3>
                                    John Size finds a winning solution for Champion’s Way as Caspar Fownes, Joao Moreira and Alexis Badel notch Sha Tin trebles
                                </h3>
                                <p>
                                    John Size’s decision to bypass Hong Kong’s lucrative LONGINES Hong Kong International Races was vindicated handsomely when Champion’s Way ended an 18-month drought by snaring the G3 Chinese Club Challenge Cup Handicap (1400m) at Sha Tin this afternoon (Friday, 1 January). Beaten by Hong Kong banner horse Golden Sixty at his three previous starts,…
                                </p>
                                <div class="toglep schedzoom" data-toggle="modal" data-target="#exampleModalLong7">
                                    Readmore...
                                </div>
                            </div>
                            <div class="modal fade" id="exampleModalLong7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog modal-xl  mw-100 w-75"  role="document">
                                    <div class="modal-content" style="background-color: transparent">
                                        <div class="modal-body">
                                            <img src="../images/blog7.png" style="width: 100%" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include '../corepage/stickyicons.php'?>

            <?php include '../corepage/footer.php'?>
    </div>
</body>
<?php include '../scripts/scripts.php'?>
<?php include '../scripts/tawktoscript.php'?>
</html>