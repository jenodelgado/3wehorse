<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div class="bghead">
            <div class="container" >
                <div class="row py-4 mt-4" >
                    <div class="col-sm-6 header-login" id="thiss">
                        <img src="../images/logo.svg" alt="">
                    </div>
                    <div class="col-sm-6 header-login" id="thiss">
                        <div class="row">
                            <div class="col-8">
                                <div class="row no-gutters">
                                    <div class="col-6 pr-2">
                                        <input type="text" name="email" id="emaillogin" class="form-control" placeholder="Username"  style="background-color: #4b515f; color: white" />
                                    </div>
                                    <div class="col-6 pr-2">
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="password" name="password" id="passwordlogin" class="form-control" placeholder="Password" style="background-color: #4b515f;  color: white" />
                                            </div>
                                            <div class="col-12 mt-1">
                                                <!-- <small style="color: gray; cursor: pointer">Forgot Password?</small> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class=" row no-gutters">
                                    <div class="col-6 pr-2 header-login-buttons-login">
                                        <button type="button" class="btn btn" id="loginme"  data-toggle="modal" data-target="#Login">
                                            <i style="color: white; font-style: unset">
                                                <img  src="images/loginvector.png" alt="">
                                                Log In
                                            </i>
                                        </button>
                                    </div>
                                    <div class="col-6 header-login-buttons-join">
                                        <button type="button" class="btn btn" data-toggle="modal" data-target="#Joinus">
                                            <i style="color: white; font-style: unset">
                                                <img src="images/joinnow.png" alt="">
                                                Join Now
                                            </i>
                                        </button>
                                        <div class="modal fade" id="Joinus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content" style="background-color: #2e323b; opacity: 1; border-radius: 10px">
                                                    <div class="modal-body">
                                                        <div class="modalforms">
                                                            <div class="modalforms-register">
                                                                <h4>REGISTRATION</h4>
                                                            </div>
                                                            <div class="modalforms-forms">
                                                                <form method="POST" >
                                                                    <div class="form-row">
                                                                        <div class="col">
                                                                            <label for="" class="labels">First Name</label>
                                                                            <input type="text" name="firstname" id="firstname" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                            <label for="" class="labels">Email Address</label>
                                                                            <input  name="email" id="email" type="email" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                            <label for="" class="labels">Password</label>
                                                                            <input  name="password" id="password" type="password" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                        </div>
                                                                        <div class="col">
                                                                            <label for="" class="labels">Last Name</label>
                                                                            <input  name="lastname" id="lastname" type="text" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                            <label for="" class="labels">Contact Number</label>
                                                                            <input  name="phonenumber" id="phonenumber" type="text" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                            <label for="" class="labels">Confirm Password</label>
                                                                            <input  type="password" class="form-control" style="background-color: #929292; border-color: #b9b3a9;color: white; margin-bottom: 10px;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="modalform-submit">
                                                                        <button type="button" class="btn btn-secondary" id="signup">Continue</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="header-logins " id="thisss">
                    <div class="logins">
                        <div class="login-3welogo">
                            <img src="../images/logo.svg" alt="">
                        </div>
                        <div class="login-username">
                            <a><i class="usersName"></i></a> 
                        </div>
                        <div class="login-datetime">
        
                        </div>
                        <div class="login-country"></div>
                        <div class="login-logout">
                            <a class="logout">
                                 <img class="ims" src="../images/logout.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="header-country">
                    <!-- <div class="header-select">
                        <select class="form-select" aria-label="Default select example">
                            <option value="1">Singapore</option>
                            <option value="2">Malaysia</option>
                            <option value="3">Macau</option>
                        </select>
                    </div> -->
                    <!-- <div class="header-country-icon">
                        <img src="./images/singaporeFlag.png" alt="">
                    </div>
                    <div class="header-country-cname">
                        Singapore
                    </div> -->
                </div>
            </div>
        </div>
</body>
</html>