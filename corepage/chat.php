<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/cssGrid.css">
    <link rel="stylesheet" href="../css/mediaQuery.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbars/">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</head>
<style>

#controls,
/* p,
span {
  margin: 0;
  padding: 0;
} */
input {
  font: 12px arial;
}
/* a {
  color: #0000FF;
  text-decoration: none;
}
a:hover {
  text-decoration: underline;
} */
#wrapper,
#loginform {
  margin: 0 auto;
  padding-bottom: 25px;
  background: #414e59;
  width: 350px;
  /* width: 504px;
  border: 1px solid #ACD8F0; */
  border-radius:10px;
}
#chatbox {
  text-align: left;
  margin: 0 auto;
  margin-bottom: 25px;
  padding: 10px;
  background: #414e59;
  height: 400px;
  /* width: 430px; */
  border: 1px solid #ACD8F0;
  overflow: auto;
  color:white;
}
#chatname {
  width: 395px;
  border: 1px solid #ACD8F0;
  margin-left: 25px;
  float: left;
}
#msg {
  /* width: 395px; */
  border: 1px solid #ACD8F0;
  border-radius: 32px;
  height: 60px;
  background-color: #414e59;
  color: white;
}
#submit {
  width: 60px;
}
#menu{
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
}
    #menu > h3{
        color: white;
    }
.ctrl{
    display: grid;
    grid-template-columns: auto 30%;
    grid-template-rows: auto;
    grid-template-areas: "inputarea send"
}
.ctrlarea{
    display: grid;
    grid-area: inputarea;
    padding-left: 10px;
}
.ctrlsend{
    display: grid;
    grid-area: send;
    padding-right: 10px;
}
</style>
<body onload="javascript:WebSocketSupport()">
  <div id="ws_support"></div>

  <div>
    <div id="wrapper">
        <div id="menu">
            <h3 class="welcome">Chat Room</h3>
        </div>

        <div id="chatbox"></div>

        <div class="ctrl" id="controls">
            <div class="ctrlarea">
                <input  name="chatname" type="hidden" id="chatname"  value="" size="67" placeholder="Type your name here" />
                <input type="text" class="form-control" id="msg"  name="msg" type="text" id="msg" size="63" placeholder="Type your message here"  placeholder="name@example.com">
            </div>
            <div class="ctrlsend">
                <img style="width:80px; padding-top: 10px;" src="../images/paperplane.png" alt="" name="sendmsg" type="submit" id="sendmsg" value="Send" onclick="doSend(document.getElementById('msg').value)">
            </div>
        </div>
    </div>
  </div>
  
</body>
<script>

var output;

var websocket;

function WebSocketSupport() {

  if (browserSupportsWebSockets() === false) {
    document.getElementById("ws_support").innerHTML = "<h2>Sorry! Your web browser does not supports web sockets</h2>";

    var element = document.getElementById("wrapper");
    element.parentNode.removeChild(element);

    return;
  }

  output = document.getElementById("chatbox");

  websocket = new WebSocket('ws://localhost:8080');

  websocket.onopen = function(e) {
    writeToScreen("");
  };


  websocket.onmessage = function(e) {
    onMessage(e)
  };

  websocket.onerror = function(e) {
    onError(e)
  };
}

function onMessage(e) {
  writeToScreen('<span style="color: blue;"> ' + e.data + '</span>');
}

function onError(e) {
  writeToScreen('<span style="color: red;">ERROR:</span> ' + e.data);
}

function doSend(message) {
  var validationMsg = userInputSupplied();
  if (validationMsg !== '') {
    alert(validationMsg);
    return;
  }
  var chatname = document.getElementById('chatname').value;

  document.getElementById('msg').value = "";

  document.getElementById('msg').focus();

  var msg = '@<b>' + chatname + '</b>: ' + message;

  websocket.send(msg);

  writeToScreen(msg);
}

function writeToScreen(message) {
  var pre = document.createElement("p");
  pre.style.wordWrap = "break-word";
  pre.innerHTML = message;
  output.appendChild(pre);
}

function userInputSupplied(){
    var msg = document.getElementById('msg').value;
    if(msg === ''){
        return 'Error!';
    }else{
        return '';
    }
}
// function userInputSupplied() {
//   var chatname = document.getElementById('chatname').value;
//   var msg = document.getElementById('msg').value;
//   if (chatname === '') {
//     return 'Please enter your username';
//   } else if (msg === '') {
//     return 'Please the message to send';
//   } else {
//     return '';
//   }
// }

function browserSupportsWebSockets() {
  if ("WebSocket" in window) {
    return true;
  } else {
    return false;
  }
}
</script>
</html>