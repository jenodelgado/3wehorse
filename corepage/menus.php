<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>
<body>
<div class="qr">
    <div class="menu container">
        <div class="tab-menus menu-home">
            <div class="tab-menus menu-home-icon">
                <img src="../images/homeicon.png" alt="">
            </div>
            <div class="tab-menus menu-home-text">
                <a href="../index.php">
                    <h6> Home </h6>
                </a>
            </div>
        </div>
        <div class="tab-menus menu-schedule">
            <a href="../pages/schedule.php">
                <h6>
                    Schedule
                </h6>
            </a>
        </div>
        <div class="tab-menus menu-livestream">
            <a href="../pages/livestream.php">
                <h6>
                        Live Streaming
                </h6>
            </a>
        </div>
        <div class="tab-menus menu-horsebet">
            <a href="../pages/bettingtips.php">
                <h6>
                    Horse Betting Tips
                </h6>
            </a>
        </div>
        <div class="tab-menus menu-horsecard">
                <a href="../pages/horseracecard.php">
                    <h6>
                        Horse Race Cards
                    </h6>
                </a>
        </div>
        <div class="tab-menus menu-testimonial">
            <a href="../pages/testimonials.php">
                <h6>
                    Testimonials
                </h6>
            </a>
        </div>
        <div class="tab-menus menu-blog">
            <a href="../pages/blogs.php">
                <h6>
                    Blogs
                </h6>
            </a>
        </div>
        <div class="tab-menus menu-horseracingaccount">
            <a href="https://www.3webet.com/page">
                <h6>
                    Horse Racing Account
                </h6>
            </a>
        </div>
        <div class="tab-menus menu-search">
            <div class="form-group has-search" style="width: 110%">
                <input type="text" class="form-control" placeholder="Search" style="background-color: #4b515f; color: white;">
            </div>
        </div>
    </div>

</div>
</body>
</html>