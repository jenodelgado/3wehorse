<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>

.icon-bar {
  position: fixed;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

.icon-bar a {
  display: block;
  text-align: center;
  padding: 16px;
  transition: all 0.3s ease;
  color: white;
  font-size: 20px;
  /* border-radius: 30px; */
}

.icon-bar a:hover {
  background-color: transparent;
}

.facebook {
  background: #3B5998;
  color: white;
}

.twitter {
  background: #55ACEE;
  color: white;
}

.google {
  background: #dd4b39;
  color: white;
}

.linkedin {
  background: #007bb5;
  color: white;
}

.youtube {
  background: #bb0000;
  color: white;
}
.telegram {
  background: gray;
  color: white;
}
.webet {
  background: rgb(97, 90, 58);
  color: white;
}
.plus{
  background: red;
  color: black;
}
.minus{
  background: black;
  color: white;
}
.webet > i :hover{
    background-color:black;
}
.tiktok {
  background: #b10491;
  color: white;
}

.content {
  margin-left: 75px;
  font-size: 30px;
}
.wechat{
  background: #7BB32E;
  color: white;
}

.fbzoom{
  background: #3B5998;
  color: white;
}
  .fbzoom:hover {
      -ms-transform: scale(1.1); 
      -webkit-transform: scale(1.1);
      transform: scale(1.1);
      color: #3B5998;
  }

.tgzoom{
  background: gray;
  color: white;
}
  .tgzoom:hover {
      -ms-transform: scale(1.1); 
      -webkit-transform: scale(1.1);
      transform: scale(1.1);
      color: gray;
  }

.twtzoom{
  background: #55ACEE;
  color: white;
}
  .twtzoom:hover {
      -ms-transform: scale(1.1); 
      -webkit-transform: scale(1.1);
      transform: scale(1.1);
      color: #55ACEE;
  }
.ytzoom{
  background: #bb0000;
  color: white;
}
  .ytzoom:hover {
      -ms-transform: scale(1.1); 
      -webkit-transform: scale(1.1);
      transform: scale(1.1);
      color: #bb0000;
  }
  .w3zoom{
    background: rgb(97, 90, 58);
    color: white;
}
  .w3zoom:hover {
      -ms-transform: scale(1.1); 
      -webkit-transform: scale(1.1);
      transform: scale(1.1);
      color: #bb0000;
  }
  .tikzoom{
    background: #b10491;
  color: white;
}
  .tikzoom:hover {
      -ms-transform: scale(1.1); 
      -webkit-transform: scale(1.1);
      transform: scale(1.1);
      color: white;
  }

  .wezoom{
    background:  #7BB32E;
  color: white;
}
  .wezoom:hover {
      -ms-transform: scale(1.1); 
      -webkit-transform: scale(1.1);
      transform: scale(1.1);
      color: #7BB32E;
  }
  .whatsappzoom{
    background:#128C7E;
    color: white;
}
  .whatsappzoom:hover {
      -ms-transform: scale(1.1); 
      -webkit-transform: scale(1.1);
      transform: scale(1.1);
      color: #128C7E;
  }

  .box{
        float:left;
        overflow: hidden;
        background: #f0e68c;
    }
    /* Add padding and border to inner content
    for better animation effect */
    .box-inner{
        width: 100px;
        height: 300px;
        padding: 10px;
        border: 1px solid #a29415;
    }

    #categories {
    display: none;
    border : 1px solid #000;
    width: 200px;
}
/* div {
    display : inline-block;
} */
.holdingbox {
    position: relative;
    top: 0;
    /* margin-left: 100px; */
}

.leftbox {
    position: relative;
    top: 0;
    left: 0;
    display: inline-block;
    font-size: 24px;
    /* background-color: #ac193d; */
    color: #FFF;
    font-weight: bold;
    padding: 1px;
    padding: 0;
    margin: 0;
}
.rightbox {
    position: relative;
    display: inline-block;
    overflow: hidden;
    width: 0;
    height: 30px;
    vertical-align: top;
    margin-right: 0;

}
.content{
    /* width: 300px; */
    position: absolute;
    /* background-color: #ac193d; */
    height: 100%;
    left: 0;
    top: 0;
    right: 0;
    color: #FFF;
    /* padding-left: 5px; */
    padding: 0;
    margin: 0;
}
.stickys{
  width: 50px; 
  height: 200px; 
  background-color: black;
  border-top-right-radius: 30px;
  border-bottom-right-radius: 30px;
  border-top: 5px solid #d8b35b;
  border-right: 5px solid #d8b35b;
  border-bottom: 5px solid #d8b35b;
  /* border: 5px solid #d8b35b; */
}
.stickys > p{
  writing-mode: vertical-lr;
  padding-top: 10px;
}

.stickys > i{
  padding: 5px;
}

.iconss{
  display: grid;
  grid-template-columns: auto ;
  grid-template-rows: auto auto auto auto auto auto auto auto ;
  grid-template-areas: "icon1" "icon2" "icon3" "icon4" "icon5" "icon6" "icon7" "icon8";
}
.iconss-1{
  grid-area: icon1;
}
.iconss-2{
  grid-area: icon2;
}
.iconss-3{
  grid-area: icon3;
}
.iconss-4{
  grid-area: icon4;
}
.iconss-5{
  grid-area: icon5;
}
.iconss-6{
  grid-area: icon6;
}
.iconss-7{
  grid-area: icon7;
}
.iconss-8{
  grid-area: icon8;
}
.icondiv{

}
</style>
<body>



<div class="icon-bar">
<div class="holdingbox">
  <span class="rightbox">
  <span class="content">
    <div class="iconss">
      <div class="icons1">
        <div class="icondiv">
          <a class="fbzoom" href="https://www.facebook.com/3wehorse" class="facebook" target="_blank"><i class="fa fa-facebook"></i>
            <h6>@3wehorse</h6>
         </a> 
        </div>
      </div>
      <div class="icons2">
        <div class="icondiv">
          <a class="tgzoom" href="https://t.me/sg3wehorse" class="telegram"  target="_blank"><i class="fa fa-telegram"></i>
            <h6>@sg3wehorse</h6>
          </a> 
        </div>
      </div>
      <div class="icons3">
          <div class="icondiv">
          <a class="twtzoom" href="https://twitter.com/3wehorse1" class="twitter"  target="_blank"><i class="fa fa-twitter"></i>
            <h6> @3wehorse1</h6>
          </a> 
          </div>
      </div>
      <div class="icons4">
          <div class="icondiv">
          <a class="ytzoom" href="https://www.youtube.com/channel/UCul4D-mYIICn8LzAQVeUNdQ" class="youtube"  target="_blank"><i class="fa fa-youtube"></i>
          <h6>@3wehorse</h6>
          </a> 

          </div>
      </div>
      <div class="icons5">
          <div class="icondiv">
          <a class="w3zoom" href="https://www.3webet.com" class="webet"  target="_blank"><i><img src="../images/3webet.png" alt="" style="width: 30px"></i>
            <h6> 3webet.com</h6>
          </a> 
          </div>
      </div>
      <div class="icons6">
          <div class="icondiv">
          <a class="tikzoom" href="https://www.tiktok.com/@3wehorse" class="tiktok"  target="_blank"><i><img src="../images/tiktok.png" alt="" style="width: 30px"></i>
            <h6>@3wehorse</h6>
          </a> 

          </div>
      </div>
      <div class="icons7">
          <div class="icondiv">
          <a class="wezoom" href="https://weixin://dl/chat?x3webetadmin"  target="_blank"><i class="fa fa-weixin" aria-hidden="true"></i>
          <h6>
            x3webetadmin
          </h6>
          </a> 

          </div>
      </div>
      <div class="icons8">
          <div class="icondiv">
          <a class="whatsappzoom" href="https://web.whatsapp.com/send?phone=6581348160&text=Hello"  target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i>
          <h6 style="text-align: revert">SG: +65 8134 8160 <br> MY: +60 11-7263 2654 </h6>
          </a>

          </div>
      </div>
    </div>
  </span>
  </span>
  <span class="leftbox">
  <div class="stickys">
    <p>contact us</p>
    <i class="fa fa-phone"></i>
  </div>
  </span>
</div>
    
  <!-- <a id="minus" class="minus" style="border-radius: 30px; width:60px"><i class="fa fa-minus"></i></a> 
  <a id="hide" class="plus" style="border-radius: 30px; width:60px"><i class="fa fa-plus"></i></a> 
  <div class="ics"> 
  <a class="tgzoom" href="https://t.me/sg3wehorse" class="telegram"  target="_blank"><i class="fa fa-telegram"></i></a> 
  <a class="twtzoom" href="https://twitter.com/3wehorse1" class="twitter"  target="_blank"><i class="fa fa-twitter"></i></a> 
  <a class="ytzoom" href="https://www.youtube.com/channel/UCul4D-mYIICn8LzAQVeUNdQ" class="youtube"  target="_blank"><i class="fa fa-youtube"></i></a> 
  <a class="w3zoom" href="https://www.3webet.com" class="webet"  target="_blank"><i><img src="../images/3webet.png" alt="" style="width: 30px"></i></a> 
  <a class="tikzoom" href="https://www.tiktok.com/@3wehorse" class="tiktok"  target="_blank"><i><img src="../images/tiktok.png" alt="" style="width: 30px"></i></a> 
  <a class="wezoom" href="https://weixin://dl/chat?x3webetadmin"  target="_blank"><i class="fa fa-weixin" aria-hidden="true"></i></a> 
  <a class="whatsappzoom" href="https://web.whatsapp.com/send?phone=6581348160&text=Hello"  target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
  </div> -->
</div>


</body>

<script>
    $('.holdingbox').hover(function(){
        $('.rightbox').stop().animate({width: '150px', height: '800px'}, 500)
    }, function(){
        $('.rightbox').stop().animate({width: '-0'}, 500)
  });
</script>

<script>
$(document).ready(function(){
  var rid = $("#hide");
  $(".ics").hide();
  $(".minus").hide();
  $("#hide").click(function(){
    $(".ics").show();
    $(".minus").show();
    $(".plus").hide();
  });

});

  $(document).ready(function($){
    $("#minus").click(function(){
    $('.ics').hide();
    $(".plus").show();
    $(".minus").hide();
  })
  })
</script>
</html> 
