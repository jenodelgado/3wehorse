<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../css/cssGrid.css">
    <link rel="stylesheet" href="../css/mediaQuery.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbars/">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <title>Privacy</title>
</head>
<body>
    <?php include '../corepage/header.php'?>
    <?php include '../corepage/menus.php'?>
    <div class="wehorsecontent">
        <div class="container fotr" style="background-color: #f3fcfb">
            <h1>Privacy</h1>
            <p>
            3WEBET is committed to protecting your privacy. The Company uses the information collected about you to track your gaming system account and game play and to provide a more personalized game playing experience. Please read on for more details about 3WEBET's privacy policy.

            When you participate in our for real money games, The Company needs to collect your personal information such as your real name, email address, and mailing address etc... This allows The Company to allow you access to the games you wish to participate in and to reward prizes that you win or money that you withdraw from your 3WEBET account. In addition, a portion of the services offered on the Website require you to provide information as a condition of usage. These services include access to participation in sweepstakes or contests, and access to subscription-only areas (if any). You may choose not to provide personally identifiable information, but this may lead to decreased functionality of the Website for you, and/or the inability to provide you with certain products or services. If you complete a transaction on our Website using a credit card, the credit card processor may collect your information for verification.

            Pixels, clear GIF's, cookies, and other technology to track how and when visitors use the Website may be used. Cookies make web surfing easier by storing your passwords and other preferences for you, and identify the IP address of your computer. Cookies also help identify which areas of the Website are popular and which are not, which may then be used to improve the user experience on the Website. You can set your browsers to reject cookies and cookies are not necessary to visit the Website; however, some functions and services will not be available unless you enable cookies on your browser.            </p>
            <p style="color: violet">Use of Information</p>
            <p>
            Your personal information will not be sold to any third party groups.

            The Company may release account information when The Company believes, in good faith, that such release is reasonably necessary to (i) comply with law, (ii) enforce or apply the terms of any user agreements or (iii) protect the rights, property or safety of 3WEBET, 3WEBET players, or others or (iv) your information is provided to comply with a service you requested on the site. In addition, The Company also reserves the right to use the information collected to occasionally notify you about important functionality changes to the Web site, new services, and special offers you'll find valuable.

            When you submit a bug, suggestion, complaint, question, request, or any other email correspondence, The Company asks for your email address so that you can receive a response.

            If you win a prize or other promotional feature, The Company may ask for your real name, mailing address, and e-mail address so that you can be notified and arrange for delivery.

            By registering for an account, you agree that 3WEBET may display your user name and game play records. From time to time, (and usually when you win a prize), The Company may ask you to voluntarily provide general demographic and other personal information for inclusion on the 3WEBET "Winners" page.

            When you play games or access your account information, 3WEBET offers the use of a secure server. The secure socket layer (SSL) encrypts all information you input before it is sent to 3WEBET or sent to you. Furthermore, all of the customer data collected is protected against unauthorized access through the most up-to-date industry-standard security measures. That being said absolute security of any information displayed online cannot be guaranteed.
            </p>
            <p style="color: violet">Third Party Web Sites</p>
            <p>
            The Website may contain links to other websites that are owned and operated by third parties. The information practices of those other websites are not covered by Top Bet's Policy. We are not responsible for the privacy policies of other websites. We strongly suggest that you review such third party's privacy policies before providing any information to them. You should contact these entities directly with questions about their use of the information that they collect.

            Opting In, Opting Out, and Modifying Information
            By opting to receive email from through the Website, you are opting in to receive by electronic mail information and/or offers on any new promotions, events, products or materials from our clients for whom we manage this policy.

            If at any time you indicate that you no longer want to receive such email communications, or if you want to change or correct any previously provided information we maintain, you may send such direction to the contact information below and such changes will be made ASAP or by logging in and going to your "my account" section on the website and deselecting the opt in box under the settings tab.

            As a security precaution, we may ask that you provide identifying information such as name, address, and/or birth date in order to confirm your identity. In addition, access to personally identifiable information may be limited or denied where providing such access would be unreasonably burdensome or expensive in the circumstances, or as otherwise permitted by law.
            </p>
            <p style="color: violet">Changes to this Policy</p>
            <p>
            By using the Gaming System, you consent to the collection and use of this information by 3WEBET. If the 3WEBET privacy policy changes, those changes will be posted on this page so that you are always aware of what information is collected, how the information is used, and under what circumstances the information is disclosed.
            </p>
        </div>
    </div>
    <?php include '../corepage/footer.php'?>
</body>
<?php include '../scripts/scripts.php'?>
<?php include '../scripts/tawktoscript.php'?>
</html>