<?php
    $user = 'root'; 
    $password = '';  
    $database = '3wehorse';  
    $servername='localhost'; 
    $mysqli = new mysqli($servername, $user,  
                    $password, $database); 
    if ($mysqli->connect_error) { 
        die('Connect Error (' .  
        $mysqli->connect_errno . ') '.  
        $mysqli->connect_error); 
    } 

    $sql = "SELECT * FROM sun_schedule ORDER BY id DESC "; 
    $result = $mysqli->query($sql); 
    $mysqli->close();
    
?>
<style>
    .styleddiv{
        height: 100px;
        width: 100px;
        position: absolute;
        bottom: 29px;
        font-size: 35px;
        color: red;
        font-weight: bolder;
        text-shadow: 2px 2px #f2f2f2;
        animation-name: example;
        animation-duration: 60s;
      
    }
    .styleddivtxt4{
        height: 100px;
        width: 100px;
        position: absolute;
        top: 20px;
        font-size: 20px;
        color: #ffffff;
        font-weight: bolder;
        text-shadow: 2px 2px #000000;
        margin-left: 10px
        /* animation-name: example;
        animation-duration: 60s; */
      
    }

    @keyframes example {
        0%   {color: red;}
        25%  {color: yellow;}
        50%  {color: blue;}
        100% {color: green;}
    }
    .tdess{
        table-layout: auto;
        width: 240px;
        margin-left: 84px;
        border-radius: 10px;
    }
    .datezoom{
        /* height: 250px; */
    }
    .datezoom:hover {
        -ms-transform: scale(1.1); 
        -webkit-transform: scale(1.1);
        transform: scale(1.1); 
    }
    .clock{
        width: 30px;
        height:30px;
    }
</style>
<div class="card">
    <img src="./images/raceevent1.png" style="width: 100%; height: auto;">
        <div class="styleddiv datezoom">
            <img src="./images/image.png" alt="" style="height: 100px; width: 78px">
            <p class="styleddivtxt3">
                SUN
            </p>
        </div>
    <table class="table tdess tdes">
        <thead>
        </thead>
        <tbody>
            
            <!-- <tr style="text-align: center">
                <td style="font-weight: bold">Location</td>
                <td style="font-weight: bold">Time</td>
            </tr> -->
            <tr></tr>
            <?php
                while($rows=$result->fetch_assoc()) 
                { 
            ?> 
            <tr> 
                <td style="text-align: center; align: justify" ><?php echo $rows['_country'];?></td> 
                <td> <img class="clock" src="./images/clock.png" alt=""></td>

                <td style="text-align: center" ><?php echo $rows['_time'];?></td> 
            </tr> 
            <?php 
                } 
            ?> 
        </tbody>
    </table>
</div>