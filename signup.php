<!DOCTYPE html>
<html lang="en">
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        require __DIR__.'../constring/connect.php';
    ?>
    <div class="container">
        <form method="POST" id="api_signup">
            <div class="form-group">
                <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" />
            </div>
            <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email" />
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" id="password" placeholder="Password" />
            </div>
            <button type="submit" class="btn btn-primary" id="button_action" value="submit">Submit</button>
        </form>
    </div>
</body>
    
<script type="text/javascript">
    $(document).ready(function(){
        jQuery.support.cors = true; 
            $('#button_action').on('click', function(e){
                e.preventDefault();
                let name = $('#name').val()
                let email = $('#email').val()
                let password = $('#password').val()
                    $.ajax({
                    method:'POST',
                    dataType: 'JSON',
                    url:"http://localhost/3wehorse/api/register.php",
                    // data: JSON.stringify({"name" : "JEno","email" : "123123@gmail.com","password":"123qweqwe123"}),
                    data: JSON.stringify({ name, email, password}),
                    }).done(function(response){
                    console.log(response);
                });
        });
    })
</script>
</html>